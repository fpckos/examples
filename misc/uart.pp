program UART;

{$mode objfpc}

uses
  SysUtils, StrUtils;

type
  TPortBuffer = packed record
    Port   : THandle;
    Address: Pointer;
    Size   : DWord;
  end;

const
  SRV_GETVERSION = 0;
  PORT_OPEN      = 1;
  PORT_CLOSE     = 2;
  PORT_RESET     = 3;
  PORT_SETMODE   = 4;
  PORT_GETMODE   = 5;
  PORT_SETMCR    = 6;
  PORT_GETMCR    = 7;
  PORT_READ      = 8;
  PORT_WRITE     = 9;

const
  ComPort: DWord = 1;

var
  Control   : TKosDriver;
  Result    : Longint;
  ApiVersion: Longint;
  HPort     : THandle;
  Buffer    : TPortBuffer;
  ReadChunk : array[Byte] of Char;
  Size      : Longint;
  Chunk     : String;
  Line      : String;
  CI, CJ    : Integer;

function LoadDriver(const Name: AnsiString): THandle;
begin
  Result := kos_loaddriver(PChar(Name));

  if Result = 0 then
  begin
    WriteLn(StdErr, 'Can''t load driver ' + UpCase(Name));
    Halt(1);
  end;

  WriteLn('Driver ' + UpCase(Name) + ' loaded');
end;


procedure WriteUART(const Data: String);
var
  Result: Longint;
begin
  if Length(Data) = 0 then Exit;

  Buffer.Port    := HPort;
  Buffer.Address := @Data[1];
  Buffer.Size    := Length(Data);

  Control.Func       := PORT_WRITE;
  Control.Data       := @Buffer;
  Control.DataSize   := SizeOf(Buffer);
  Control.Result     := nil;
  Control.ResultSize := 0;

  Result := kos_controldriver(@Control);

  if Result < 0 then
  begin
    Sleep(10);
    Result := kos_controldriver(@Control);
  end;

  if Result < 0 then
  begin
    WriteLn(StdErr, 'Failure write');
    Halt(1);
  end;
end;


function ReadUART(): String;
begin
  Buffer.Address := @ReadChunk;
  Buffer.Size    := SizeOf(ReadChunk) - 1;

  Control.Func       := PORT_READ;
  Control.Data       := @Buffer;
  Control.DataSize   := SizeOf(Buffer);
  Control.Result     := @Size;
  Control.ResultSize := SizeOf(Size);

  while True do
  begin
    if (kos_controldriver(@Control) < 0) or (Size < 0) or (SizeOf(ReadChunk) <= Size) then
    begin
      WriteLn(StdErr, 'Failure read');
      Halt(1);
    end;
    if 0 < Size then Break;
    Sleep(20);
  end;

  ReadChunk[Size] := #0;
  Result := StrPas(PChar(ReadChunk));
end;


function FilterChars(const Src: String): String;
var
  I: Integer;
begin
  Result := '';
  for I := 1 to Length(Src) do
  case Src[I] of
    #8, #10, #13, #127: Result := Result + Src[I];
    #32..#126: Result := Result + Src[I];
  end;
end;


begin
  Control.Handle     := LoadDriver('UART'); { @xxx: обязательно в UpperCase, т.е. регистр должен соотв.
                                                    регистру имени драйвера или драйвер будет загружаться повторно,
                                                    так работает сравнение имён в ядре (см. dll/get_service)!!! }
  Control.Func       := SRV_GETVERSION;
  Control.Data       := nil;
  Control.DataSize   := 0;
  Control.Result     := @ApiVersion;
  Control.ResultSize := SizeOf(ApiVersion);

  Result := kos_controldriver(@Control);

  if Result < 0 then
  begin
    WriteLn(StdErr, 'Failure get driver version');
    Halt(1);
  end;

  WriteLn('Driver API version: ', ApiVersion);

  {--------------------}

  Control.Func       := PORT_OPEN;
  Control.Data       := @ComPort;
  Control.DataSize   := SizeOf(ComPort);
  Control.Result     := @HPort;
  Control.ResultSize := SizeOf(HPort);

  Result := kos_controldriver(@Control);

  if Result < 0 then
  begin
    WriteLn(StdErr, 'Failure open port');
    Halt(1);
  end;

  WriteLn('Port opened');

  {--------------------}

  WriteUART('Hello World!!!'#13#10);
  WriteUART('> ');

  Line := '';

  while True do
  begin
    Chunk := ReadUART();
    Chunk := FilterChars(Chunk);

    CI := Pos(#13, Chunk);

    if CI = 0 then CI := Pos(#10, Chunk);
    if 0 < CI then Delete(Chunk, CI, Length(Chunk) - CI + 1);

    Chunk := DelChars(Chunk, #10);

    while True do
    begin
      CJ := Pos(#8, Chunk);

      if CJ = 0 then CJ := Pos(#127, Chunk);
      if CJ = 0 then Break;

      if 1 < CJ then
        Delete(Chunk, CJ - 1, 2) else
      begin
        if 0 < Length(Line) then
        begin
          WriteUART(#8' '#8);
          Delete(Line, Length(Line), 1);
        end;
        Delete(Chunk, CJ, 1);
      end;
    end;

    if 250 < Length(Line) + Length(Chunk) then
      Delete(Chunk, 1 + 250 - Length(Line), Length(Line) + Length(Chunk) - 250);

    Line := Line + Chunk;

    WriteUART(Chunk);

    if 0 < CI then Break;
  end;

  WriteUART(#13#10);
  WriteLn('Input (', Length(Line), '): "', Line, '"');

  {--------------------}

  Control.Func       := PORT_CLOSE;
  Control.Data       := @HPort;
  Control.DataSize   := SizeOf(HPort);
  Control.Result     := nil;
  Control.ResultSize := 0;

  Result := kos_controldriver(@Control);

  if Result < 0 then
  begin
    WriteLn(StdErr, 'Failure close port');
    Halt(1);
  end;

  WriteLn('Port closed');
  WriteLn('Goodbye');
end.
