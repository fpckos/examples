program Hello;

uses
  SysUtils;

var
  F: File;
  L, J: Longint;
  P: Pointer;
begin
  WriteLn(Format('Hello World (%s)!!!', [DateTimeToStr(Now)]));

  WriteLn('ParamStr(0) :: ', ParamStr(0));
  WriteLn('ExpandFileName :: ', ExpandFileName(ParamStr(0)));

  L := FileAge('lorem ipsum');
  WriteLn('FileAge(1) :: ', L);
//WriteLn('FileAge(1) :: ', DateTimeToStr(FileDateToDateTime(L)));  kos: EConvertError
  L := FileAge(ParamStr(0));
  WriteLn('FileAge(2) :: ', L);
  WriteLn('FileAge(2) :: ', DateTimeToStr(FileDateToDateTime(L)));

  Assign(F, ChangeFileExt(ParamStr(0), '.pp'));
  Reset(F, 1);
  L := FileSize(F);
  P := GetMem(L);

  BlockRead(F, P^, L, J);
  WriteLn('BlockRead :: ', L, ' -> ', J);
  BlockRead(F, P^, L, J);
  WriteLn('BlockRead :: ', L, ' -> ', J);

  FreeMem(P);
  Close(F);

  Assign(F, 'lorem ipsum, lorem ipsum');
  Reset(F, 1);
end.
