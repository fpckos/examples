program Xpc1;

{$mode objfpc}
{-$i-}

uses
  SysUtils, Classes, Process;

var
  I: Integer;
  BasePath   : String;
  FileExt    : String;
  CommandLine: String;
  CurrDir : String;
  Env     : TStrings;
  Proc    : TProcess;
begin
  BasePath := ExtractFilePath(ParamStr(0));
  FileExt  := ExtractFileExt(ParamStr(0));

  GetDir(0, CurrDir);
  WriteLn('[xpc1] Current Dir : ', CurrDir);
  WriteLn('[xpc1] ParamStr(0) : ', ParamStr(0));

  WriteLn('[xpc1] Change Dir  : ', BasePath);
  ChDir(BasePath);     { SetCurrentDir(BasePath); }
  GetDir(0, CurrDir);  { CurrDir := GetCurrentDir; }
  WriteLn('[xpc1] Current Dir : ', CurrDir);

  for I := 1 to GetEnvironmentVariableCount do
    WriteLn('[xpc1] ', GetEnvironmentString(I));

  CommandLine := BasePath + 'xpc2' + FileExt + ' -arg1 -arg2=value2';
  WriteLn('[xpc1] Execute xpc2: ', CommandLine);

  { Если, как в этом примере, передаются переменные окружения, то любый другие переменные (глобальные или от этого процесса)
    игнорируются. В противном случае, будут переданы пеерменные данного процесса. (Эта логика, специфичная для TProcess.) }

  Env := TStringList.Create;
  Env.Append('PATH=' + BasePath);
  Env.Append('PARENT=(' + IntToStr(GetProcessID) + ')' + ParamStr(0));
  Env.Append('MESSAGE=Hello from xpc1 ($PATH)');
  Env.Append('COMMA=1,2,3');
  Env.Append('QUOTE1=1''2''''3');
  Env.Append('QUOTE2=1"2""3');

 {Env.CommaText -> PATH=/hd0/1/examples/misc/:/RD/1,PARENT=(19)/hd0/1/examples/misc/xpc1.kex,"MESSAGE=Hello from xpc1","COMMA=1,2,3","QUOTE1=1""2",QUOTE2=1'2 }

  Proc := TProcess.Create(nil);
  Proc.CommandLine := CommandLine;
  Proc.Environment := Env;
  Proc.Execute;

  WriteLn('[xpc1] Wait xpc2...');
  Proc.WaitOnExit;

  Env.Free;
  Proc.Free;

  WriteLn('[xpc1] Exit');
end.
