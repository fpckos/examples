program Uart2;

{$mode objfpc}

uses
  SysUtils,
  Uart;

var
  T1: Text;
  T2: Text;
  S : String;
begin
  AssignUART(T1, 1);
  Append(T1);
  WriteLn(T1, '[1/3] Output ready: ', TextRec(T1).Name);

  AssignUART(T2, 1);
  Reset(T2);

  WriteLn(T1, '[2/3] Input ready: ', TextRec(T2).Name);
  Write(T1, '> '); Flush(T1);
  ReadLn(T2, S);
  WriteLn(T1, S);

  WriteLn(T1, '[3/3] Goodbye');

  Close(T1);
  Close(T2);

  WriteLn('Exit');
end.
