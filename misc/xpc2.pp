program Xpc2;

uses
{$ifdef UNIX}CThreads,{$endif}
  SysUtils;


procedure HexDump(Addr: Pointer; Start, Stop: Longint);
var
  O, W   : Longint;
  PC     : PChar;
  S1, S2 : String;
begin
  S1 := '';
  S2 := '';
  PC := PChar(Addr + Start);
  W  := 0;

  for O := 0 to Stop - Start - 1 do
  begin
    if 0 < W then
      S1 := S1 + ' ';

    S1 := S1 + IntToHex(Byte(PC^), 2);
    if (#32 <= PC^) and (PC^ <= #126) then S2 := S2 + PC^ else S2 := S2 + '.';

    Inc(PC);
    Inc(W);

    if W = 8 then
    begin
      WriteLn('[xpc2]   [', S1, '] "', S2, '"');
      S1 := '';
      S2 := '';
      W  := 0;
    end;
  end;

  if 0 < W then
    WriteLn('[xpc2]   [', S1, '] "', S2, '"');
end;

function ThreadBody(Param: Pointer): PtrInt;
begin
  WriteLn('[xpc2] Hello from thread #', Longint(Param));
  ThreadBody := 0;
end;

var
  I: Integer;
begin
  WriteLn('[xpc2] Process ID : ', GetProcessID);
  WriteLn('[xpc2] Current Dir: ', GetCurrentDir);
  WriteLn('[xpc2] StackTop   : ', IntToHex(DWord(StackTop), 8));
  HexDump(StackTop, 1*8, 4*8);

  for I := 1 to GetEnvironmentVariableCount do
    WriteLn('[xpc2] ', GetEnvironmentString(I));

{$ifdef Kolibri}
  for I := Low(KosXpcData) to High(KosXpcData) do
    WriteLn('[xpc2] $', KosXpcData[I].Name, '=', KosXpcData[I].Value);
{$endif}

  BeginThread(@ThreadBody, Pointer(1));
  BeginThread(@ThreadBody, Pointer(2));

  for I := 5 downto 1 do
  begin
    Write(#13, I, '...');
    if 1 < I then Sleep(500);
  end;

  WriteLn(#13'[xpc2] Exit');

  { @note: Linux не ждёт завершения потоков, я тоже не жду, если не использовать флаг THREAD_NODAEMON.
           Например, см. `kos_term.inc`. }
end.
