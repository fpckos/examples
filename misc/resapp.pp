{$codepage utf8}

{ Ожидается такой вывод (как в Linux, так и в KolibriOS):
  EnumResourceTypes:
  - $00000003
  - $0000000A
  - $0000000E
  - $00000010
  EnumResourceNames($0003):
  - $00000001 = 1
  - $00000002 = 2
  - $00000003 = 3
  - $00000004 = 4
  - $00000005 = 5
  - $00000006 = 6
  - $00000007 = 7
  - $00000008 = 8
  - $00000009 = 9
  - $0000000A = 10
  - $0000000B = 11
  - $0000000C = 12
  - $0000000D = 13
  - $0000000E = 14
  EnumResourceNames($000A):
  - $0042FBE1 = "IMAGE"
  EnumResourceNames($000E):
  - $0042FBE7 = "APP1"
  - $0042FBEC = "APP2"
  - $0042FBF1 = "APP3"
  - $0042FBF6 = "MAINICON"
  EnumResourceNames($0010):
  - $00000001 = 1
  FindResource(1, $0003): Size=744
  FindResource(2, $0003): Size=296
  FindResource(3, $0003): Size=304
  FindResource(4, $0003): Size=1384
  FindResource(5, $0003): Size=2984
  FindResource(6, $0003): Size=3752
  FindResource(7, $0003): Size=744
  FindResource(8, $0003): Size=296
  FindResource(9, $0003): Size=9640
  FindResource(10, $0003): Size=7336
  FindResource(11, $0003): Size=4264
  FindResource(12, $0003): Size=2216
  FindResource(13, $0003): Size=1128
  FindResource(14, $0003): Size=1384
  FindResource("IMAGE", $000A): Size=18793
  FindResource("APP1", $000E): Size=48
  FindResource("APP2", $000E): Size=34
  FindResource("APP3", $000E): Size=90
  FindResource("MAINICON", $000E): Size=48
  FindResource(1, $0010): Size=724
  [IMAGE] Resource Size: 18793
}

program ResApp;

{$mode objfpc}
{$apptype console}
{-$resource resapp.rc}
{$resource resapp.res}
{$resource mainicon.res}

uses
  ResUnit;

{resourcestring
  SResourceStringTest = 'ResourceStringTest';}

{ RT_ICON=3 RT_RCDATA=10 RT_GROUP_ICON=14 RT_VERSION=16 }

{------------------------------------------------------------------------------}

function InternalIsIntResource(aStr: PChar; var aInt: PtrUint): Boolean;
var
  I   : Integer;
  S   : ShortString;
  Code: Word;
begin
  { @note: (!) для Kolibri это может стать проблемой (хоть и маловероятно, что FPC программа будет меньше 64kb :-)
           (!) правда, если будут (а точно будут!) использоваться прекомпилированные строки... (см. "IMAGE" в самом низу)
               вроде что-то сделал по этому поводу, см. `sysres.inc` }
  { @note: что-бы сохранить формат строк (в Windres WideString-и, а нужны ASCIIZ), буду выделять память (см. `kos_winres.inc`) }

  InternalIsIntResource := (PtrUInt(aStr) shr 16) = 0;

  if InternalIsIntResource then
    aInt := PtrUInt(aStr) else
  if aStr[0] = '#' then
  begin
    I := 1;
    while aStr[i] <> #0 do Inc(I);
    if 256 < I then I := 256;
    S[0] := Chr(I - 1);
    Move(aStr[1], S[1], I - 1);
    Val(S, aInt, Code);
    InternalIsIntResource := Code = 0;
  end;
  { @todo: (!) else }
end;

{------------------------------------------------------------------------------}

var
  ResourceTypes: array of PChar;
  Resources    : array of record ResourceType: PChar; ResourceName: PChar; end;

function EnumResType(ModuleHandle: TFPResourceHMODULE; ResourceType: PChar; lParam: PtrInt): LongBool; stdcall;
begin
  WriteLn('- $', HexStr(PtrUInt(ResourceType), 8));
  SetLength(ResourceTypes, Length(ResourceTypes) + 1);
  ResourceTypes[High(ResourceTypes)] := ResourceType;
  Result := True;
end;

function EnumResName(ModuleHandle: TFPResourceHMODULE; ResourceType, ResourceName: PChar; lParam: PtrInt): LongBool; stdcall;
var
  Id: PtrUInt;
begin
  Write('- $', HexStr(PtrUInt(ResourceName), 8), ' = ');

  if InternalIsIntResource(ResourceName, Id) then
    WriteLn(Id) else
    WriteLn('"', ResourceName, '"');

  SetLength(Resources, Length(Resources) + 1);
  Resources[High(Resources)].ResourceType := ResourceType;
  Resources[High(Resources)].ResourceName := ResourceName;

  Result := True;
end;

var
  HResource: TResourceHandle;
  Handle   : THandle;
  I        : Integer;
  Id       : PtrUInt;
{$ifdef KOLIBRI}
  RootTable: Pointer; external name 'FPC_WINRESTABLE';
{$endif}
begin
{$ifdef KOLIBRI}
  WriteLn('RootTable: $', HexStr(PtrUInt(RootTable),    8));  { @ex: $010000 }
  WriteLn('StackTop : $', HexStr(PtrUInt(StackTop),     8));  { @ex: $050A7C }
  WriteLn('GetMem   : $', HexStr(PtrUInt(GetMem(1024)), 8));  { @ex: $4D5040 }
{$endif}

  WriteLn('EnumResourceTypes:');
  if not EnumResourceTypes(HInstance, @EnumResType, 0) then
    WriteLn('- Unexpected result / Error');

  for I := Low(ResourceTypes) to High(ResourceTypes) do
  begin
    WriteLn('EnumResourceNames($', HexStr(PtrUInt(ResourceTypes[I]), 4),'):');
    if not EnumResourceNames(HInstance, ResourceTypes[I], @EnumResName, 0) then
      WriteLn('- Unexpected result / Error');
  end;

  for I := Low(Resources) to High(Resources) do
  with Resources[I] do
  begin
    Write('FindResource(');
    if InternalIsIntResource(ResourceName, Id) then
      Write(Id) else
      Write('"', ResourceName, '"');
    Write(', $', HexStr(PtrUInt(ResourceType), 4),'): ');

    HResource := FindResource(HInstance, ResourceName, ResourceType);
    Assert(HResource <> 0);

    Handle := LoadResource(HInstance, HResource);
    Assert(Handle <> 0);

    WriteLn('Size=', SizeOfResource(HInstance, HResource));
  end;

  {----------------------------------------------------------------------------}

  HResource := FindResource(HInstance, 'IMAGE', RT_RCDATA);
  if HResource = 0 then
    WriteLn(StdErr, '[IMAGE] Resource not found') else
  begin
    Handle := LoadResource(HInstance, HResource);
    if Handle = 0 then
      WriteLn(StdErr, '[IMAGE] Resource found, but can''t be loaded');
    WriteLn('[IMAGE] Resource Size: ', SizeOfResource(HInstance, HResource));
  end;
end.
