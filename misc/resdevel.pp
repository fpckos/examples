{$codepage utf8}

program ResDevel;

{$mode objfpc}
{$apptype console}
{-$resource resapp.rc}
{$resource resapp.res}
{$resource mainicon.res}

uses
  SysUtils, Classes, RTLConsts,
  Math,
  ResUnit;

{resourcestring
  SResourceStringTest = 'ResourceStringTest';}

type
  PResDirTable = ^TResDirTable;
  TResDirTable = packed record
    Characteristics  : Longword;
    TimeStamp        : Longword;
    VerMajor         : Word;
    VerMinor         : Word;
    NamedEntriesCount: Word;
    IDEntriesCount   : Word;
  end;

  PResDirEntry = ^TResDirEntry;
  TResDirEntry = packed record
    NameID       : Longword;
    DataSubDirRVA: Longword;
  end;

  PResDataEntry = ^TResDataEntry;
  TResDataEntry = packed record
    DataRVA : Longword;
    Size    : Longword;
    Codepage: Longword;
    Reserved: Longword;
  end;


procedure ReadNodeDirEntry(Base: Pointer; Entry: PResDirEntry; Indent: String); forward;
procedure ReadResDataEntry(Base: Pointer; Entry: PResDataEntry; Indent: String); forward;


function ReadResString(Base: Pointer; aRVA: Longword): String;
var
  ws: WideString;
  wl: Word;
begin
  wl := PWord(Base + aRVA)^;
  SetString(ws, PWideChar(Base + aRVA + 2), wl);
  Result := ws;
end;


procedure ReadNodeTable(Base: Pointer; Table: PResDirTable; Indent: String='');
var
  P: Pointer;
  I: Word;
begin
  P := Pointer(Table) + SizeOf(Table^);

  WriteLn(Indent + 'NamedEntriesCount: ', Table^.NamedEntriesCount);

  for I := 1 to Table^.NamedEntriesCount do
  begin
    WriteLn(Indent + '- NamedEntry[', I, ']');
    ReadNodeDirEntry(Base, PResDirEntry(P), Indent + '  ');
    Inc(P, SizeOf(TResDirEntry));
  end;

  WriteLn(Indent + 'IDEntriesCount   : ', Table^.IDEntriesCount);

  for I := 1 to Table^.IDEntriesCount do
  begin
    WriteLn(Indent + '- IDEntry[', I, ']');
    ReadNodeDirEntry(Base, PResDirEntry(P), Indent + '  ');
    Inc(P, SizeOf(TResDirEntry));
  end;
end;

procedure ReadNodeDirEntry(Base: Pointer; Entry: PResDirEntry; Indent: String='');
var
  Name: String;
begin
  if (Entry^.NameID and $80000000) <> 0 then
    WriteLn(Indent + 'Name/ID      : ', ReadResString(Base, Entry^.NameID and $7FFFFFFF)) else
    WriteLn(Indent + 'Name/ID      : ', Entry^.NameID, ' ($', IntToHex(Entry^.NameID, 8), ')');
  WriteLn(Indent + 'DataSubDirRVA: $', IntToHex(Entry^.DataSubDirRVA, 8));

  if (Entry^.DataSubDirRVA and $80000000) <> 0 then
    ReadNodeTable(Base, PResDirTable(Base + (Entry^.DataSubDirRVA and $7FFFFFFF)), Indent) else
    ReadResDataEntry(Base, PResDataEntry(Base + Entry^.DataSubDirRVA), Indent);
end;

procedure ReadResDataEntry(Base: Pointer; Entry: PResDataEntry; Indent: String='');
var
  P   : PChar;
  I, C: Longword;
  HLine, CLine: String;
begin
  WriteLn(Indent + 'Size    : ', Entry^.Size);
  WriteLn(Indent + 'Codepage: ', Entry^.Codepage, ' ($', IntToHex(Entry^.Codepage, 8), ')');
  WriteLn(Indent + 'DataRVA : $', IntToHex(Entry^.DataRVA, 8));

  P := PChar(Entry^.DataRVA);
  C := Min(32, Entry^.Size);

  HLine := '';
  CLine := '';

  for I := 1 to C do
  begin
    HLine := HLine + IntToHex(Byte(P^), 2);
    if (#32 <= P^) and (P^ <= #126) then CLine := CLine + P^ else CLine := CLine + '.';

    if (I = C) or (I = 16) then
    begin
      WriteLn(Indent, '[', HLine, '] ', CLine);
      HLine := '';
      CLine := '';
    end else
      HLine := HLine + ' ';

    Inc(P);
  end;

  if 32 < Entry^.Size then
    WriteLn(Indent, '[...]');
end;


var
  RootTable: PResDirTable; external name 'FPC_WINRESTABLE';
begin
  WriteLn('HInstance: ', HInstance);
  WriteLn('RootTable: ', IntToHex(DWord(RootTable), 8));

  if not Assigned(RootTable) then
    raise EResNotFound.Create('Resources does not exists');

  { RT_ICON=3 RT_RCDATA=10 RT_GROUP_ICON=14 }

  ReadNodeTable(RootTable, RootTable);
end.
