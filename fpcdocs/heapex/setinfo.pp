program heapex;

{ Program used to demonstrate the usage of heaptrc unit }

uses heaptrc;

var P1: PLongint;
    P2: Pointer;
    I : longint;
    Marker : Longint;

procedure SetMarker(P: Pointer);
begin
  PLongint(P)^ := Marker;
end;

procedure ShowMarker(var T: Text; P: Pointer);
begin
  WriteLn(T, 'Marker: $', HexStr(PLongint(P)^, 8));
end;

procedure Part1;
begin
  // Blocks allocated here are marked with $FFAAFFAA = -5570646
  Marker := $FFAAFFAA;
  New(P1);
  New(P1);
  Dispose(P1);

  for I := 1 to 10 do
  begin
    GetMem(P2, 128);
    If (I mod 2) = 0 Then FreeMem(P2, 128);
  end;

  GetMem(P2, 128);
end;

procedure Part2;
begin
  // Blocks allocated here are marked with $FAFAFAFA = -84215046
  Marker := $FAFAFAFA;
  New(P1);
  New(P1);
  Dispose(P1);

  for I := 1 to 10 do
  begin
    GetMem(P2, 128);
    If (I mod 2) = 0 Then FreeMem(P2, 128);
  end;

  GetMem(P2, 128);
end;

begin
  SetHeapExtraInfo(SizeOf(Marker), @SetMarker, @ShowMarker);
  Writeln('Part 1');
  Part1;
  Writeln('Part 2');
  Part2;
  WriteLn('Exit');
end.
